import React, { FunctionComponent, useState } from 'react'
import { GiHamburgerMenu } from 'react-icons/gi'
import { NavLink } from 'react-router-dom'
import './navbar.css'

export const NavBar: FunctionComponent = () => {
  const [open, setOpen] = useState(false)

  const changeOpen = (): void => {
    setOpen(!open)
  }

  const renderMenu = (): JSX.Element|undefined => {
    if (open) {
      return (
        <div id='HamMenu'>
          <NavLink to='/' className='MenuItemLink'>
            <div className='MenuItem'>
              Home
            </div>
          </NavLink>
          <NavLink to='/popular/' className='MenuItemLink'>
            <div className='MenuItem'>
              Popular
            </div>
          </NavLink>
          <NavLink to='/t/' className='MenuItemLink'>
            <div className='MenuItem'>
              Tags
            </div>
          </NavLink>
        </div>
      )
    }
  }

  return (
    <div className='Menu'>
      <div className='NavBar'>
        <div onClick={changeOpen} className='IconButton'>
          <GiHamburgerMenu size={32} className='Icon' />
        </div>
      </div>
      {renderMenu()}
    </div>
  )
}

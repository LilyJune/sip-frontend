import React, { FunctionComponent } from 'react'

interface PreviewProps {
  id: number
  imageUrl: string
  title: string
  rating: number
}

export const Preview: FunctionComponent<PreviewProps> = ({ id, imageUrl, title, rating }) => (
  <div className='Preview'>
    <img className='PreviewImage' src={imageUrl} alt={title} />
    <div className='PreviewText'>
      <span>{title}</span><span>{rating}</span>
    </div>
  </div>
)

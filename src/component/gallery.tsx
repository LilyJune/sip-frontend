import React, { FunctionComponent } from 'react'
import forEach from 'lodash.foreach'
import { Preview } from './preview'

interface GalleryProps {
  title: string
  entries: object
}

interface Entry {
  id: number
  imageUrl: string
  title: string
  rating: number
}

export const Gallery: FunctionComponent<GalleryProps> = ({ title, entries }) => (
  <div className='Gallery'>
    <h1>{title}</h1>
    {forEach(entries, (entry: Entry) => <Preview id={entry.id} imageUrl={entry.imageUrl} title={entry.title} rating={entry.rating} />)}
  </div>
)

import decode from 'jwt-decode'
const API_SERVER_URL = 'http://localhost:8080/'

interface tokenObject {
  token: string
}

interface decodedToken {
  id: number
  username: string
  name: string
  exp: number
}

export class HTTPService {
  onChange: Function

  constructor (onChange: Function = () => {}) {
    this.onChange = onChange
  }

  async getPopular (): Promise<Response> {
    return await this._makeRequest('gallery', {
      method: 'GET'
    }).then((res: Response) => {
      return res
    })
  }

  async getEntry (id: string): Promise<Response> {
    return await this._makeRequest(`e/${id}`, {
      method: 'GET'
    }).then((res: Response) => {
      return res
    })
  }

  async login (username: string, password: string): Promise<tokenObject> {
    return await this._makeRequest('login', {
      method: 'POST',
      body: JSON.stringify({
        username: username,
        password: password
      })
    }).then((res: tokenObject) => {
      this.setToken(res.token)
      return res
    })
  }

  loggedIn (): boolean {
    const token = this.getToken()
    return token !== '' && token !== null && !this.isTokenExpired(token)
  }

  isTokenExpired (token: string): boolean {
    try {
      const decoded: decodedToken = decode(token)
      return (decoded.exp < Date.now() / 1000)
    } catch (err) {
      console.warn(err)
      return false
    }
  }

  setToken (token: string): void {
    localStorage.setItem('id', token)
    this._handleChange()
  }

  getToken (): string {
    const rawToken: any = localStorage.getItem('id')
    if (typeof (rawToken) === 'string') {
      return rawToken
    } else {
      return ''
    }
  }

  getName (): string {
    if (this.loggedIn()) {
      try {
        const token: string = this.getToken()
        const decodedToken: decodedToken = decode(token)
        return decodedToken.name
      } catch (err) {
        console.warn(err)
      }
    }
    return ''
  }

  getUsername (): string {
    if (this.loggedIn()) {
      try {
        const token: string = this.getToken()
        const decodedToken: decodedToken = decode(token)
        return decodedToken.username
      } catch (err) {
        console.warn(err)
      }
    }
    return ''
  }

  getID (): number {
    if (this.loggedIn()) {
      try {
        const token: string = this.getToken()
        const decodedToken: decodedToken = decode(token)
        return decodedToken.id
      } catch (err) {
        console.warn(err)
      }
    }
    return -1
  }

  logout (): void {
    localStorage.removeItem('id')
    this._handleChange()
  }

  _handleChange (): void {
    if (this.onChange !== null) this.onChange()
  }

  async _makeRequest (url: string, options: RequestInit): Promise<any> {
    options.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }

    if (this.loggedIn()) {
      options.headers.Authorization = `Bearer ${this.getToken()}`
    }

    return await fetch(API_SERVER_URL + url, options).then(async (response: Response) => {
      if (response.ok) {
        return await response.json()
      } else {
        return await response.json().then(({ errors }) => {
          const error = new Error(response.statusText)
          throw error
        })
      }
    })
  }
}

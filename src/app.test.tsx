import React from 'react'
import { shallow } from 'enzyme'
import App from './app'
import { HTTPService } from './service/http-service'

describe('<App />', () => {
  it('renders', () => {
    const http: HTTPService = new HTTPService()
    shallow(<App httpService={http} />)
  })
})

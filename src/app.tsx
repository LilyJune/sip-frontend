import React, { FunctionComponent } from 'react'
import { BrowserRouter, Route, Switch, RouteComponentProps } from 'react-router-dom'
import { HomePage } from './page/home'
import { ErrorPage } from './page/error'
import { HTTPService } from './service/http-service'
import { EntryPage } from './page/entry/entry'
import { NavBar } from './component/navbar'
import { NewEntryPage } from './page/entry/new-entry'

interface AppProps {
  httpService: HTTPService
}

interface MatchParams {
  id: string
}

type MatchProps = RouteComponentProps<MatchParams>

const App: FunctionComponent<AppProps> = ({ httpService }) => {
  return (
    <div className='App'>
      <BrowserRouter>
        <NavBar />
        <Switch>
          <Route exact path='/' render={() => <HomePage httpService={httpService} />} />
          <Route path='/e/new/' component={({ match }: MatchProps) => <NewEntryPage httpService={httpService} />} />
          <Route path='/e/:id/' component={({ match }: MatchProps) => <EntryPage httpService={httpService} id={match.params.id} />} />
          <Route render={() => <ErrorPage errorNumber={404} />} />
        </Switch>
      </BrowserRouter>
    </div>
  )
}

export default App

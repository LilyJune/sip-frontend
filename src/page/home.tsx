import React, { FunctionComponent, useState, useEffect } from 'react'
import { HTTPService } from '../service/http-service'
import { Gallery } from '../component/gallery'

interface HomePageProps {
  httpService: HTTPService
}

export const HomePage: FunctionComponent<HomePageProps> = ({ httpService }) => {
  const [entries, setEntries] = useState({})
  useEffect(() => {
    const getEntries: () => void = async () => {
      if (Object.keys(entries).length <= 0) {
        const data = await httpService.getPopular()
        setEntries(data)
      }
    }
    getEntries()
  })

  const renderGallery: Function = (): any => {
    if (entries instanceof Response) {
      return <Gallery title='Entries' entries={entries} />
    }
  }

  return (
    <div id='Home'>
      <h1>Home</h1>
      {renderGallery()}
    </div>
  )
}

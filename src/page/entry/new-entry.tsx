import React, { FunctionComponent, useState, FormEvent, ChangeEventHandler, useEffect } from 'react'
import { HTTPService } from '../../service/http-service'

interface PageProps {
  httpService: HTTPService
}

declare interface PromiseConstructor {
  allSettled(promises: Array<Promise<any>>): Promise<Array<{status: 'fulfilled' | 'rejected', value?: any, reason?: any}>>
}

export const NewEntryPage: FunctionComponent<PageProps> = ({ httpService }) => {
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [groupID, setGroupID] = useState(-1)
  const [files, setFiles] = useState<File[]>([])
  const [previews, setPreviews] = useState<JSX.Element[]>([])

  useEffect(() => {
    if (files !== null) {
      const promises: Array<Promise<(string | ArrayBuffer | null)>> = []
      for (let i = 0; i < files.length; i++) {
        promises.push(getURL(files[i]))
      }
      Promise.all(promises).then((urls) => {
        const pV = urls.map((fileURL: (string | ArrayBuffer | null), i) => {
          return <img src={String(fileURL)} key={i + 1} alt={`Page: ${i + 1}`} />
        })
        setPreviews(pV)
      }).catch((err) => window.alert(err))
    }
  }, [files])

  const getURL = async (file: File): Promise<(string | ArrayBuffer | null)> => {
    return await new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onloadend = () => {
        resolve(reader.result)
      }
    })
  }

  const handleAddFile: ChangeEventHandler<HTMLInputElement> = (e): void => {
    const { target: { files } } = e
    if (files !== null) {
      if (files.length > 0) {
        setFiles(Array.from(files))
      }
    }
  }

  const handleSubmit = (e: FormEvent<HTMLFormElement>): void => {
    e.preventDefault()

    const state = {
      title: title,
      description: description,
      group_id: groupID,
      files: files
    }

    console.log(state)
  }

  return (
    <div className='NewEntry'>
      <form onSubmit={handleSubmit}>
        <label>
          Title:<br />
          <input
            type='text'
            value={title}
            onChange={e => setTitle(e.target.value)}
          />
        </label><br /><br />
        <label>
          Description:<br />
          <textarea
            value={description}
            onChange={e => setDescription(e.target.value)}
          />
        </label><br /><br />
        <label>
          Group ID:<br />
          <input
            type='number'
            value={groupID}
            onChange={e => setGroupID(Number(e.target.value))}
          />
        </label><br /><br />
        <label>
          Upload Pages:<br />
          <input
            type='file'
            onChange={handleAddFile}
            accept='image/png'
            multiple
          />
        </label><br /><br />
        {previews}<br />
        <input type='submit' value='Submit' />
      </form>
    </div>
  )
}

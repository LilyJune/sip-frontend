import React, { FunctionComponent, useState, useEffect } from 'react'
import { HTTPService } from '../../service/http-service'
import './entry.css'

interface EntryPageProps {
  httpService: HTTPService
  id: string
}

interface Entry {
  title: string
  description: string
  group_id: number | null
  id: number
  pages: number
  createdAt: string
  updatedAt: string
}

const initEntry: Entry = {
  title: '',
  description: '',
  group_id: null,
  id: -1,
  pages: 0,
  createdAt: '',
  updatedAt: ''
}

export const EntryPage: FunctionComponent<EntryPageProps> = ({ httpService, id }) => {
  const [entry, setEntry] = useState(initEntry)
  const [read, setRead] = useState(false)
  useEffect(() => {
    const getEntry: () => void = async () => {
      const res: unknown = await httpService.getEntry(id)
      const resEntry: Entry = res as Entry

      if (resEntry !== null) {
        setEntry(resEntry)
      }
    }
    if (entry === initEntry) {
      getEntry()
    }
  })

  const showPages = (): JSX.Element[] => {
    const pages = []
    for (let i = 0; i < entry.pages; i++) {
      pages.push(<img key={i} className='Page' src={`http://localhost:8080/p/${id}/${i + 1}/`} alt={`Page ${i + 1}`} />)
    }
    return pages
  }

  const changeRead = (): void => {
    setRead(true)
  }

  const renderPages = (): JSX.Element[]|undefined => {
    if (read) {
      return showPages().map((entry) => {
        return entry
      })
    }
  }

  return (
    <div className='Entry'>
      <div className='EntryDetailsContainer'>
        <div className='EntryDetails'>
          <div className='EntryDetailsAlign'>
            <div className='EntryDetailsAlignLeft'>
              <h1>{entry.title}</h1>
              <p>Pages: {entry.pages}</p>
              <p>Posted On: {entry.createdAt}</p>
              <p>Last Updated On: {entry.updatedAt}</p>
            </div>
            <div className='EntryDetailsAlignRight'>
              <br />
              <h2>Description</h2>
              <p>{entry.description}</p>
            </div>
          </div>
          <div className='right'>
            <button className='ReadButton' onClick={changeRead}>Read Now</button>
          </div>
        </div>
      </div>
      <hr />
      <div className='PageContainer'>
        {renderPages()}
      </div>
    </div>
  )
}

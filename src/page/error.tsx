import React, { FunctionComponent } from 'react'

interface ErrorPageProps {
  errorNumber: Number
}

export const ErrorPage: FunctionComponent<ErrorPageProps> = ({ errorNumber }) => {
  return (
    <div id='Error'>
      <h1>{errorNumber}</h1>
    </div>
  )
}
